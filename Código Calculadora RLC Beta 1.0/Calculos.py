from math import atan,cos,sin

pi=3.141592653589793

def CalculosSerie(V,f,R,L,C):
    V_Bin=complex(V,0)

    F_Res=Resonancia(L,C)

    XL=Reactancia_IN(f,L)
    XL_Bin=complex(0,XL)
    XC=Reactancia_CAP(f,C)
    XC_Bin=complex(0,XC)

    ZT_Bin=complex(R,XL+XC)
    ZT_Mod=abs(ZT_Bin)
    ZT_Ang=arctg(R,XL+XC)

    IT_Bin=V_Bin/ZT_Bin
    IT_Mod=abs(IT_Bin)
    IT_Ang=-ZT_Ang

    if R!=0:
        VR_Bin=IT_Bin*R
        VR_Mod=abs(VR_Bin)
        VR_Ang=IT_Ang
    else:
        VR_Bin=0
        VR_Mod=0
        VR_Ang=0

    if L!=0:
        VL_Bin=IT_Bin*XL_Bin
        VL_Mod=abs(VL_Bin)
        VL_Ang=IT_Ang+90
    else:
        VL_Bin=0
        VL_Mod=0
        VL_Ang=0

    if C!=0:
        VC_Bin=IT_Bin*XC_Bin
        VC_Mod=abs(VC_Bin)
        VC_Ang=IT_Ang-90
    else:
        VC_Bin=0
        VC_Mod=0
        VC_Ang=0
    
    FP=coseno(ZT_Ang)
    S=V*IT_Mod
    P=S*FP
    Q=S*seno(ZT_Ang)
    
    return (F_Res,XL,XC,ZT_Bin,ZT_Mod,ZT_Ang,IT_Bin,IT_Mod,IT_Ang,
            VR_Bin,VR_Mod,VR_Ang,VL_Bin,VL_Mod,VL_Ang,VC_Bin,VC_Mod,VC_Ang,
            S,P,Q,FP)

def CalculosParalelo(V,f,R,L,C):
    V_Bin=complex(V,0)

    F_Res=Resonancia(L,C)

    XL=Reactancia_IN(f,L)
    XL_Bin=complex(0,XL)
    XC=Reactancia_CAP(f,C)
    XC_Bin=complex(0,XC)

    if R==0:
        G=0
    else:
        G=1/R
            
    if L==0:
        BL=0
    else:
        BL=-1/XL
            
    if C==0:
        BC=0
    else:
        BC=-1/XC
            
    B=BC+BL
            
    ZT_Bin=1/(complex(G,B))
    ZT_Mod=abs(ZT_Bin)
    ZT_Ang=arctg(ZT_Bin.real,ZT_Bin.imag)

    IT_Bin=V_Bin/ZT_Bin
    IT_Mod=abs(IT_Bin)
    IT_Ang=-ZT_Ang

    if R!=0:
        IR_Bin=V_Bin/R
        IR_Mod=abs(IR_Bin)
    else:
        IR_Bin=0
        IR_Mod=0
    IR_Ang=0 #El ángulo de la corriente en R es siempre 0

    if L!=0:
        IL_Bin=V_Bin/XL_Bin
        IL_Mod=abs(IL_Bin)
        IL_Ang=-90 #El ángulo es siempre -90
    else:
        IL_Bin=0
        IL_Mod=0
        IL_Ang=0

    if C!=0:
        IC_Bin=V_Bin/XC_Bin
        IC_Mod=abs(IC_Bin)
        IC_Ang=90 #El ángulo es siempre 90
    else:
        IC_Bin=0
        IC_Mod=0
        IC_Ang=0

    FP=coseno(ZT_Ang)
    S=V*IT_Mod
    P=S*FP
    Q=S*seno(ZT_Ang)

    return (F_Res,XL,XC,ZT_Bin,ZT_Mod,ZT_Ang,IT_Bin,IT_Mod,IT_Ang,
            IR_Bin,IR_Mod,IR_Ang,IL_Bin,IL_Mod,IL_Ang,IC_Bin,IC_Mod,IC_Ang,
            S,P,Q,FP)

def Resonancia (inductancia,capacitancia):
    if inductancia == 0 or capacitancia == 0:
        F_Res=0
    else:
        F_Res=(1/(2*pi*(inductancia*capacitancia)**0.5))
    return F_Res

def Reactancia_IN(frecuencia,inductancia):
    XL=(2*pi*frecuencia*inductancia)
    return XL

def Reactancia_CAP(frecuencia,capacitancia):
    if capacitancia == 0:
        XC=0
    else:
        XC=(-1/(2*pi*frecuencia*capacitancia))
    return XC

def arctg(X,Y):             #Arcotangente en grados sexagesimales.
    if X!=0:
        if X<0 and Y<0:         #Corrección cuadrantal de la función.
            ArcoTangente=(atan(Y/X)*180/pi)-180
        elif X<0 and Y>0:
            ArcoTangente=(atan(Y/X)*180/pi)+180
        else:
            ArcoTangente=(atan(Y/X)*180/pi)
    else:
        ArcoTangente=(Y/abs(Y))*90
    return ArcoTangente

def coseno(Ang):            #Coseno en grados sexagesimales.
    if Ang==90 or Ang==-90:
        Res=0
    else:
        Res=cos(Ang*pi/180)
    return Res

def seno(Ang):              #Seno en grados sexagesimales.
    if Ang==0 or Ang==180 or Ang==-180:
        Res=0
    else:
        Res=sin(Ang*pi/180)
    return Res
