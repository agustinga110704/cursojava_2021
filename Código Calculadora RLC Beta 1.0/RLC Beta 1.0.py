from tkinter import Tk,Frame,Label,PhotoImage,Button,Entry,Checkbutton,font,filedialog,messagebox,Spinbox,Menu,IntVar,StringVar,END,ttk
import clipboard
from Calculos import CalculosSerie,CalculosParalelo
from ImagenesRLC import *

Red=4   #Dígitos de redondeo
SeparadorDecimal=1 #Separador decimal de los resultados: 0=punto // 1=coma
ConfigAngulos=0 #Configuración de los ángulos: 0=grados con decimales // 1= grados, minutos y segundos
SignoAngulos=0 #Signo de los ángulos: 0=positivos y negativos // 1=solo positivos

class App:
    def __init__ (self,master):
        self.master=master
        
        self.master.title("Calculadora RLC")
        self.master.resizable(False,False)
        self.master.config(bg="#B0E4ED")

        self.master.geometry("+10+10") #Ajusta la ventana en la esquina superior izquierda (x=10;y=10)

        self.master.unbind_all("<<NextWindow>>") #Desactiva la función predeterminada de la tecla TAB.

        global imagenLogo,imagenLogoChico,imagenSerie,imagenParalelo

        self.logo=PhotoImage(data=LogoSTR)
        self.master.iconphoto(True,self.logo)

        imagenLogo=self.logo.subsample(5)

        imagenLogoChico=self.logo.subsample(12)

        self.circuitoSerie=PhotoImage(data=SerieSTR)
        imagenSerie=self.circuitoSerie.subsample(3)

        self.circuitoParalelo=PhotoImage(data=ParaleloSTR)
        imagenParalelo=self.circuitoParalelo.subsample(3)

        global estiloLinea          #Estilo del separator.
        estiloLinea = ttk.Style(self.master)
        estiloLinea.configure("TSeparator", background="#98acd4")

        #Menú click derecho: Cortar/Copiar/Pegar.
        self.master.bind_class("Entry","<ButtonRelease-3>",lambda event:App.MenuPopup(self,event))

        App.menuClickDer=Menu(self.master,tearoff=False)
        App.menuClickDer.add_command(label="Cortar",command=lambda:App.Cortar(self))
        App.menuClickDer.add_command(label="Copiar",command=lambda:App.Copiar(self))
        App.menuClickDer.add_command(label="Pegar",command=lambda:App.Pegar(self))

        #Bindings de Control X/C/V: Se cambia las funciones por defecto a las del programa.
        #Esto se hace principalmente para utilizar el módulo clipboard. Los datos copiados con este módulo quedan guardados al cerrar la aplicación
        #(a diferencia del portapapeles de tkinter).
        self.master.bind_class("Entry","<Control-x>",lambda _:App.Cortar(self))
        self.master.bind_class("Entry","<Control-c>",lambda _:App.Copiar(self))
        self.master.bind_class("Entry","<Control-v>",lambda _:App.Pegar(self))

        self.Actual=Inicio(self.master)
        self.master.mainloop()

    def MenuPopup(self,event):  #Evento que se activa al hacer click derecho en un Entry.
        event.widget.focus_set()
        estado=str(event.widget["state"])
        if estado=="readonly" or estado=="disabled":  #En Entries de resultados las opciones "Cortar" y "Pegar" aparecen desactivadas.
            App.menuClickDer.entryconfig("Cortar", state="disabled")
            App.menuClickDer.entryconfig("Pegar", state="disabled")
        else:
            App.menuClickDer.entryconfig("Cortar", state="normal")
            App.menuClickDer.entryconfig("Pegar", state="normal")
        
        App.menuClickDer.tk_popup(event.x_root,event.y_root)
    
    def Cortar(self,event=None):
        EntrySel=self.master.focus_get()
        try:
            seleccion=EntrySel.selection_get()
            clipboard.copy(seleccion)
            EntrySel.delete("sel.first","sel.last") #Se borra desde el inicio hasta el final de la selección.
        except: #Gracias a esto no salta en consola el error generado al no haber ninguna selección.
            pass

    def Copiar(self,event=None):
        EntrySel=self.master.focus_get()
        try:
            seleccion=EntrySel.selection_get()
            clipboard.copy(seleccion)
        except:
            pass

    def Pegar(self,event=None):
        EntrySel=self.master.focus_get()
        portapapeles=clipboard.paste()
        try:
            EntrySel.delete("sel.first","sel.last")
        except:
            pass
        EntrySel.insert("insert",portapapeles)

    def Datos(self,frameA):     #Labels y Entries en común. frameB = Ingreso de datos en común entre circuitos.
        #Frame interno
        self.frameB=Frame(frameA,bg="#B0E4ED")
        self.frameB.grid(row=1,column=0,sticky="nw",padx=10)

        #Label de subtítulo
        self.datosLabel=Label(self.frameB,text="Datos ingresados:",bg="#B0E4ED",font=("Open Sans Bold", 12),anchor="w",justify="left")
        self.datosLabel.grid(row=0,column=0,columnspan=2,sticky="w",padx=1)

        #Tensión
        self.VLabel=Label(self.frameB,text="Tensión (V) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.VLabel.grid(row=2,column=0,sticky="e",padx=2,pady=1)

        App.VEntry=Entry(self.frameB,width=20,font=("Open Sans", 10)) #Solo se especifica el width para este entry. El resto se adapta con sticky="we"
        App.VEntry.grid(row=2,column=1,padx=2,pady=1,sticky="we")

        #Frecuencia
        self.fLabel=Label(self.frameB,text="Frecuencia (f) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.fLabel.grid(row=3,column=0,sticky="e",padx=2,pady=1)

        App.fEntry=Entry(self.frameB,font=("Open Sans", 10))
        App.fEntry.grid(row=3,column=1,padx=2,pady=1,sticky="we")

        #Resistencia
        self.RLabel=Label(self.frameB,text="Resistencia (R) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.RLabel.grid(row=4,column=0,sticky="e",padx=2,pady=1)

        App.REntry=Entry(self.frameB,font=("Open Sans", 10))
        App.REntry.grid(row=4,column=1,padx=2,pady=1,sticky="we")

        #Inductancia
        self.LLabel=Label(self.frameB,text="Inductancia (L) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.LLabel.grid(row=5,column=0,sticky="e",padx=2,pady=1)

        App.LEntry=Entry(self.frameB,font=("Open Sans", 10))
        App.LEntry.grid(row=5,column=1,padx=2,pady=1,sticky="we")

        #Capacitancia
        self.CLabel=Label(self.frameB,text="Capacitancia (C) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.CLabel.grid(row=6,column=0,sticky="e",padx=2,pady=1)

        App.CEntry=Entry(self.frameB,font=("Open Sans", 10))
        App.CEntry.grid(row=6,column=1,padx=2,pady=1,sticky="we")

    def IngresoDatos(self,frameA):
        App.frameA=frameA
        self.multiplos=("G","M","K","","m","μ","n","p")

        App.Datos(self,frameA)

        self.frameB.config(pady=15)

        self.datosLabel.grid_forget()

        #Label de datos inválidos
        self.errorLabel=Label(self.frameB,bg="#B0E4ED",font=("Open Sans Bold", 10),anchor="w",justify="left")
        
        #Tensión
        self.VMultiploIN=ttk.Combobox(self.frameB,values=self.multiplos,width=3,state="readonly",font=("Open Sans", 10))
        self.VMultiploIN.current(3)
        self.VMultiploIN.grid(row=2,column=2,sticky="w",padx=2,pady=1)
        self.VMultiploIN.bind("<<ComboboxSelected>>",lambda _:App.VEntry.focus_set()) #Fijar el focus en en Entry correspondiente para que funcione el ENTER y no se vea azul el Combobox.

        self.VUnidadLabel=Label(self.frameB,text="V",bg="#B0E4ED",font=("Open Sans", 10))
        self.VUnidadLabel.grid(row=2,column=3,sticky="w",padx=2,pady=1)

        self.VCorrectoLabel=Label(self.frameB,bg="#B0E4ED",fg="white",font=("Open Sans Bold", 10))
        self.VCorrectoLabel.grid(row=2,column=4,padx=2,pady=1)

        App.VEntry.focus_set() #Al inicio, el focus está fijado en el Entry de tensión.

        #Frecuencia
        self.fMultiploIN=ttk.Combobox(self.frameB,values=self.multiplos,width=3,state="readonly",font=("Open Sans", 10))
        self.fMultiploIN.current(3)
        self.fMultiploIN.grid(row=3,column=2,sticky="w",padx=2,pady=1)
        self.fMultiploIN.bind("<<ComboboxSelected>>",lambda _:App.fEntry.focus_set())

        self.fUnidadLabel=Label(self.frameB,text="Hz",bg="#B0E4ED",font=("Open Sans", 10))
        self.fUnidadLabel.grid(row=3,column=3,sticky="w",padx=2,pady=1)

        self.fCorrectoLabel=Label(self.frameB,bg="#B0E4ED",fg="white",font=("Open Sans Bold", 10))
        self.fCorrectoLabel.grid(row=3,column=4,padx=2,pady=1)

        #Resistencia
        self.RMultiploIN=ttk.Combobox(self.frameB,values=self.multiplos,width=3,state="readonly",font=("Open Sans", 10))
        self.RMultiploIN.current(3)
        self.RMultiploIN.grid(row=4,column=2,sticky="w",padx=2,pady=1)
        self.RMultiploIN.bind("<<ComboboxSelected>>",lambda _:App.REntry.focus_set())

        self.RUnidadLabel=Label(self.frameB,text="Ω",bg="#B0E4ED",font=("Open Sans", 10))
        self.RUnidadLabel.grid(row=4,column=3,sticky="w",padx=2,pady=1)

        self.RCorrectoLabel=Label(self.frameB,bg="#B0E4ED",fg="white",font=("Open Sans Bold", 10))
        self.RCorrectoLabel.grid(row=4,column=4,padx=2,pady=1)

        #Inductancia
        self.LMultiploIN=ttk.Combobox(self.frameB,values=self.multiplos,width=3,state="readonly",font=("Open Sans", 10))
        self.LMultiploIN.current(3)
        self.LMultiploIN.grid(row=5,column=2,sticky="w",padx=2,pady=1)
        self.LMultiploIN.bind("<<ComboboxSelected>>",lambda _:App.LEntry.focus_set())

        self.LUnidadLabel=Label(self.frameB,text="H",bg="#B0E4ED",font=("Open Sans", 10))
        self.LUnidadLabel.grid(row=5,column=3,sticky="w",padx=2,pady=1)

        self.LCorrectoLabel=Label(self.frameB,bg="#B0E4ED",fg="white",font=("Open Sans Bold", 10))
        self.LCorrectoLabel.grid(row=5,column=4,padx=2,pady=1)

        #Capacitancia
        self.CMultiploIN=ttk.Combobox(self.frameB,values=self.multiplos,width=3,state="readonly",font=("Open Sans", 10))
        self.CMultiploIN.current(3)
        self.CMultiploIN.grid(row=6,column=2,sticky="w",padx=2,pady=1)
        self.CMultiploIN.bind("<<ComboboxSelected>>",lambda _:App.CEntry.focus_set())

        self.CUnidadLabel=Label(self.frameB,text="F",bg="#B0E4ED",font=("Open Sans", 10))
        self.CUnidadLabel.grid(row=6,column=3,sticky="w",padx=2,pady=1)

        self.CCorrectoLabel=Label(self.frameB,bg="#B0E4ED",fg="white",font=("Open Sans Bold", 10))
        self.CCorrectoLabel.grid(row=6,column=4,padx=2,pady=1)

        #Checkbuttons "sin R/L/C"
        App.sinR=IntVar()
        self.sinRCheck=Checkbutton(self.frameB,text="Sin resistencia (R)",bg="#B0E4ED",activebackground="#B0E4ED",font=("Open Sans", 10))
        self.sinRCheck.config(variable=App.sinR,onvalue=True,offvalue=False,command=lambda:App.SinR_L_C(self,App.sinR,App.REntry,self.RMultiploIN))
        self.sinRCheck.grid(row=7,column=1,columnspan=4,padx=2,pady=(5,0),sticky="w")

        App.sinL=IntVar()
        self.sinLCheck=Checkbutton(self.frameB,text="Sin bobina (L)",bg="#B0E4ED",activebackground="#B0E4ED",font=("Open Sans", 10))
        self.sinLCheck.config(variable=App.sinL,onvalue=True,offvalue=False,command=lambda:App.SinR_L_C(self,App.sinL,App.LEntry,self.LMultiploIN))
        self.sinLCheck.grid(row=8,column=1,columnspan=4,padx=2,sticky="w")

        App.sinC=IntVar()
        self.sinCCheck=Checkbutton(self.frameB,text="Sin capacitor (C)",bg="#B0E4ED",activebackground="#B0E4ED",font=("Open Sans", 10))
        self.sinCCheck.config(variable=App.sinC,onvalue=True,offvalue=False,command=lambda:App.SinR_L_C(self,App.sinC,App.CEntry,self.CMultiploIN))
        self.sinCCheck.grid(row=9,column=1,columnspan=4,padx=2,sticky="w")

        #Detección de flechas arriba y abajo en el ingreso de datos.
        App.Entries=(App.VEntry,App.fEntry,App.REntry,App.LEntry,App.CEntry)

        App.VEntry.bind("<Up>",lambda _:App._MoverCursor(self,0,-1))
        App.VEntry.bind("<Down>",lambda _:App._MoverCursor(self,0,1))

        App.fEntry.bind("<Up>",lambda _:App._MoverCursor(self,1,-1))
        App.fEntry.bind("<Down>",lambda _:App._MoverCursor(self,1,1))

        App.REntry.bind("<Up>",lambda _:App._MoverCursor(self,2,-1))
        App.REntry.bind("<Down>",lambda _:App._MoverCursor(self,2,1))

        App.LEntry.bind("<Up>",lambda _:App._MoverCursor(self,3,-1))
        App.LEntry.bind("<Down>",lambda _:App._MoverCursor(self,3,1))

        App.CEntry.bind("<Up>",lambda _:App._MoverCursor(self,4,-1))
        App.CEntry.bind("<Down>",lambda _:App._MoverCursor(self,4,1))

        #Detección de ENTER.
        App.VEntry.bind("<Return>",lambda _:App.Calcular(self,frameA))
        App.fEntry.bind("<Return>",lambda _:App.Calcular(self,frameA))
        App.REntry.bind("<Return>",lambda _:App.Calcular(self,frameA))
        App.LEntry.bind("<Return>",lambda _:App.Calcular(self,frameA))
        App.CEntry.bind("<Return>",lambda _:App.Calcular(self,frameA))
    
    def SinR_L_C(self,variable,Entry,Combobox):     #Comando de los checkbuttons.
        if(variable.get()):
            Entry.delete(0,END)
            Entry.config(state="disabled")
            Combobox.current(3)
            Combobox.config(state="disabled")
        else:
            Entry.config(state="normal")
            Combobox.config(state="normal")

    def _MoverCursor(self,posicionActual,direccion): #Función para mover el cursor por los entries de ingreso de datos.
        if posicionActual==4 and direccion==1:
            Entry=App.VEntry
        else:
            pos=posicionActual+direccion
            Entry=App.Entries[pos]

        if Entry["state"]=="disabled": #Si el Entry está desactivado, llama a la función nuevamente para que el cursor se mueva al siguiente (o al anterior).
            App._MoverCursor(self,pos,direccion)
        else:
            Entry.focus_set()
            Entry.icursor(END)
    
    def Calcular(self,frameA): #Función principal de cálculos.
        global V,f,R,L,C
        V,VCorrecto=App.ConvertirFloat(self,App.ReemplazoComa(self,App.VEntry.get()))
        f,fCorrecto=App.ConvertirFloat(self,App.ReemplazoComa(self,App.fEntry.get()))
        
        if(App.sinR.get()):    #Comprobación de Checkbuttons
            R=0             #Si R vale 0 es que el circuito no cuenta con ese componente.
            RCorrecto=True
        else:
            R,RCorrecto=App.ConvertirFloat(self,App.ReemplazoComa(self,App.REntry.get()))
            if R==0 and (RCorrecto):
                App.sinR.set(True)
        
        if(App.sinL.get()):
            L=0             #Idem R
            LCorrecto=True
        else:
            L,LCorrecto=App.ConvertirFloat(self,App.ReemplazoComa(self,App.LEntry.get()))
            if L==0 and (LCorrecto):
                App.sinL.set(True)

        if(App.sinC.get()):
            C=0             #Idem R
            CCorrecto=True
        else:
            C,CCorrecto=App.ConvertirFloat(self,App.ReemplazoComa(self,App.CEntry.get()))
            if C==0 and (CCorrecto):
                App.sinC.set(True)
        
        #El programa no admite tensión o frecuencia igual a cero.
        if V==0:
            VCorrecto=False
        
        if f==0:
            fCorrecto=False
        
        global VMult,fMult,RMult,LMult,CMult
        VMult=self.VMultiploIN.get()
        fMult=self.fMultiploIN.get()
        RMult=self.RMultiploIN.get()
        LMult=self.LMultiploIN.get()
        CMult=self.CMultiploIN.get()

        App.LabelX(self,VCorrecto,self.VCorrectoLabel)
        App.LabelX(self,fCorrecto,self.fCorrectoLabel)
        App.LabelX(self,RCorrecto,self.RCorrectoLabel)
        App.LabelX(self,LCorrecto,self.LCorrectoLabel)
        App.LabelX(self,CCorrecto,self.CCorrectoLabel)

        if (App.sinR.get()) and (App.sinL.get()) and (App.sinC.get()):
            self.errorLabel.config(text='ERROR: El circuito debe tener por lo menos \nun componente (R, L o C).')
            self.errorLabel.grid(row=1,column=0,columnspan=5,sticky="w",padx=1,pady=2)

            self.VCorrectoLabel.config(text="",bg="#B0E4ED")    # Si no hay R, L ni C, los cuadros de error al lado de V y f
            self.fCorrectoLabel.config(text="",bg="#B0E4ED")    # no aparecen, sin importar sus valores.
        else:
            if VCorrecto==True and fCorrecto==True and RCorrecto==True and LCorrecto==True and CCorrecto==True:
                self.errorLabel.grid_forget()

                self.V=V*App.Multiplo(self,VMult)
                self.f=f*App.Multiplo(self,fMult)
                self.R=R*App.Multiplo(self,RMult)
                self.L=L*App.Multiplo(self,LMult)
                self.C=C*App.Multiplo(self,CMult)

                global esParalelo,Resultados
                if (isinstance(App.Actual,Serie)):
                    frameA.destroy()
                    esParalelo=False
                    Resultados=CalculosSerie(self.V,self.f,self.R,self.L,self.C) #Función del módulo Calculos
                    App.Actual=ResultadosSerie(self.master)
                else:
                    frameA.destroy()
                    esParalelo=True
                    Resultados=CalculosParalelo(self.V,self.f,self.R,self.L,self.C) #Función del módulo Calculos
                    App.Actual=ResultadosParalelo(self.master)
            else:
                self.errorLabel.config(text='ERROR: Los datos con una "X" son inválidos.')
                self.errorLabel.grid(row=1,column=0,columnspan=5,sticky="w",padx=1,pady=2)
                
                if V<0:
                    self.errorLabel.config(text='ERROR: Los datos con una "X" son inválidos.\nLa tensión no puede ser negativa.')

    def Resultados(self,frameA):
        self.modo=["binómica","polar"]
        self.sinRLC=" - - - -"    #String que aparece si el circuito no tiene R, L o C, en los Entry correspondientes.

        #Sección de datos ingresados
        App.Datos(self,frameA)

        if(esParalelo):
            App.VEntry.config(width=32)

        self.VIN=App.FormatoDatos(self,V) + " " + VMult + "V"
        self.fIN=App.FormatoDatos(self,f) + " " + fMult + "Hz"

        if App.sinR.get()==False:
            self.RIN=App.FormatoDatos(self,R) + " " + RMult + "Ω"
        else:
            self.RIN=self.sinRLC

        if App.sinL.get()==False:    
            self.LIN=App.FormatoDatos(self,L) + " " + LMult + "H"
        else:
            self.LIN=self.sinRLC

        if App.sinC.get()==False:
            self.CIN=App.FormatoDatos(self,C) + " " + CMult + "F"
        else:
            self.CIN=self.sinRLC

        App.VEntry.insert(0,self.VIN)
        App.VEntry.config(state="readonly")

        App.fEntry.insert(0,self.fIN)
        App.fEntry.config(state="readonly")

        App.REntry.insert(0,self.RIN)
        App.REntry.config(state="readonly")

        App.LEntry.insert(0,self.LIN)
        App.LEntry.config(state="readonly")

        App.CEntry.insert(0,self.CIN)
        App.CEntry.config(state="readonly")

        #Variables de resultados
        App.resonancia=Resultados[0]
        App.XL=Resultados[1]
        App.XC=Resultados[2]
        App.ZT_Bin=Resultados[3]
        App.ZT_Mod=Resultados[4]
        App.ZT_Ang=Resultados[5]
        App.IT_Bin=Resultados[6]
        App.IT_Mod=Resultados[7]
        App.IT_Ang=Resultados[8]
        App.EspR_Bin=Resultados[9]
        App.EspR_Mod=Resultados[10]
        App.EspR_Ang=Resultados[11]
        App.EspL_Bin=Resultados[12]
        App.EspL_Mod=Resultados[13]
        App.EspL_Ang=Resultados[14]
        App.EspC_Bin=Resultados[15]
        App.EspC_Mod=Resultados[16]
        App.EspC_Ang=Resultados[17]
        App.S=Resultados[18]
        App.P=Resultados[19]
        App.Q=Resultados[20]
        App.FP=Resultados[21]

        #Variables de salida STR (String).
        if App.sinL.get()==False and App.sinC.get()==False:
            self.resonancia_STR=(App.FormatoRes(self,App.resonancia) + "Hz")
        else:
            self.resonancia_STR=self.sinRLC

        if App.sinL.get()==False:
            self.XL_STR=(" J " + App.FormatoRes(self,App.XL) + "Ω")
        else:
            self.XL_STR=self.sinRLC
        
        if App.sinC.get()==False:
            self.XC_STR=("- J " + App.FormatoRes(self,App.XC,True) + "Ω")
        else:
            self.XC_STR=self.sinRLC
        
        #Impedancia total    
        if App.ZT_Bin.imag>=0:
            self.ZT_Bin_STR=(App.FormatoRes(self,App.ZT_Bin.real) + "Ω + J " + App.FormatoRes(self,App.ZT_Bin.imag) + "Ω")
        else:
            self.ZT_Bin_STR=(App.FormatoRes(self,App.ZT_Bin.real) + "Ω - J " + App.FormatoRes(self,App.ZT_Bin.imag,True) + "Ω")
        
        self.ZT_Polar_STR=(App.FormatoRes(self,App.ZT_Mod) + "Ω; φ = " + App.FormatoAngulos(self,App.ZT_Ang))

        #Corriente total
        if App.IT_Bin.imag>=0:
            self.IT_Bin_STR=(App.FormatoRes(self,App.IT_Bin.real) + "A + J " + App.FormatoRes(self,App.IT_Bin.imag) + "A")
        else:
            self.IT_Bin_STR=(App.FormatoRes(self,App.IT_Bin.real) + "A - J " + App.FormatoRes(self,App.IT_Bin.imag,True) + "A")
        
        self.IT_Polar_STR=(App.FormatoRes(self,App.IT_Mod) + "A; φ = " + App.FormatoAngulos(self,App.IT_Ang))

        #Especial R: Tensión o corriente
        if App.sinR.get()==False:
            if(esParalelo):
                EspR_Unidad="A"
            else:
                EspR_Unidad="V"

            if App.EspR_Bin.imag>=0:
                self.EspR_Bin_STR=(App.FormatoRes(self,App.EspR_Bin.real) + EspR_Unidad + " + J " + App.FormatoRes(self,App.EspR_Bin.imag) + EspR_Unidad)
            else:
                self.EspR_Bin_STR=(App.FormatoRes(self,App.EspR_Bin.real) + EspR_Unidad + " - J " + App.FormatoRes(self,App.EspR_Bin.imag,True) + EspR_Unidad)
        
            self.EspR_Polar_STR=(App.FormatoRes(self,App.EspR_Mod) + EspR_Unidad + "; φ = " + App.FormatoAngulos(self,App.EspR_Ang))
        else:
            self.EspR_Bin_STR=self.sinRLC
            self.EspR_Polar_STR=self.sinRLC

        #Especial L: Tensión o corriente
        if App.sinL.get()==False:
            if(esParalelo):
                EspL_Unidad="A"
            else:
                EspL_Unidad="V"

            if App.EspL_Bin.imag>=0:
                self.EspL_Bin_STR=(App.FormatoRes(self,App.EspL_Bin.real) + EspL_Unidad + " + J " + App.FormatoRes(self,App.EspL_Bin.imag) + EspL_Unidad)
            else:
                self.EspL_Bin_STR=(App.FormatoRes(self,App.EspL_Bin.real) + EspL_Unidad + " - J " + App.FormatoRes(self,App.EspL_Bin.imag,True) + EspL_Unidad)
        
            self.EspL_Polar_STR=(App.FormatoRes(self,App.EspL_Mod) + EspL_Unidad + "; φ = " + App.FormatoAngulos(self,App.EspL_Ang))
        else:
            self.EspL_Bin_STR=self.sinRLC
            self.EspL_Polar_STR=self.sinRLC

        #Especial C: Tensión o corriente
        if App.sinC.get()==False:
            if(esParalelo):
                EspC_Unidad="A"
            else:
                EspC_Unidad="V"

            if App.EspC_Bin.imag>=0:
                self.EspC_Bin_STR=(App.FormatoRes(self,App.EspC_Bin.real) + EspC_Unidad + " + J " + App.FormatoRes(self,App.EspC_Bin.imag) + EspC_Unidad)
            else:
                self.EspC_Bin_STR=(App.FormatoRes(self,App.EspC_Bin.real) + EspC_Unidad + " - J " + App.FormatoRes(self,App.EspC_Bin.imag,True) + EspC_Unidad)
        
            self.EspC_Polar_STR=(App.FormatoRes(self,App.EspC_Mod) + EspC_Unidad + "; φ = " + App.FormatoAngulos(self,App.EspC_Ang))
        else:
            self.EspC_Bin_STR=self.sinRLC
            self.EspC_Polar_STR=self.sinRLC

        #Potencias
        self.S_STR=(App.FormatoRes(self,App.S) + "V·A")
        self.P_STR=(App.FormatoRes(self,App.P) + "W")
        self.Q_STR=(App.FormatoRes(self,App.Q) + "V·Ar")
        self.FP_STR=(App.strRes(self,App.Redondeo(self,App.FP)))

        #Sección de resultados
        self.frameC=Frame(frameA,bg="#B0E4ED")
        self.frameC.grid(row=1,column=1,rowspan=2,padx=10,sticky="n")
        
        self.resultadosLabel=Label(self.frameC,text="Resultados:",bg="#B0E4ED",font=("Open Sans Bold", 12),justify="left",anchor="w")
        self.resultadosLabel.grid(row=0,column=0,columnspan=2,padx=2,sticky="w")

        #Bucle de separadores.
        self.separadores=(2,5,8,11,18)
        
        for pos in self.separadores:
            self.barra=ttk.Separator(self.frameC,style="TSeparator")
            self.barra.grid(row=pos,column=0,columnspan=2,pady=6,padx=7,sticky="we")

        #Frecuencia de resonancia
        self.resonanciaLabel=Label(self.frameC,text="Frecuencia de resonancia =",bg="#B0E4ED",font=("Open Sans", 10))
        self.resonanciaLabel.grid(row=1,column=0,sticky="e",padx=2,pady=1)

        self.resonanciaEntry=Entry(self.frameC,width=28,font=("Open Sans", 10))
        self.resonanciaEntry.grid(row=1,column=1,padx=(2,5),pady=1)
        self.resonanciaEntry.insert(0,self.resonancia_STR)
        self.resonanciaEntry.config(state="readonly")

        #Reactancias
        self.XLLabel=Label(self.frameC,text="Reactancia inductiva de L =",bg="#B0E4ED",font=("Open Sans", 10))
        self.XLLabel.grid(row=3,column=0,sticky="e",padx=2,pady=1)

        self.XLEntry=Entry(self.frameC,width=28,font=("Open Sans", 10))
        self.XLEntry.grid(row=3,column=1,padx=(2,5),pady=1)
        self.XLEntry.insert(0,self.XL_STR)
        self.XLEntry.config(state="readonly")

        self.XCLabel=Label(self.frameC,text="Reactancia capacitiva de C =",bg="#B0E4ED",font=("Open Sans", 10))
        self.XCLabel.grid(row=4,column=0,sticky="e",padx=2,pady=1)

        self.XCEntry=Entry(self.frameC,width=28,font=("Open Sans", 10))
        self.XCEntry.grid(row=4,column=1,padx=(2,5),pady=1)
        self.XCEntry.insert(0,self.XC_STR)
        self.XCEntry.config(state="readonly")

        #Frame de impedancia total (solo los label y el combobox)
        self.frameZT=Frame(self.frameC,bg="#B0E4ED")
        self.frameZT.grid(row=6,column=0,columnspan=2,sticky="we")

        self.ZTLabel=Label(self.frameZT,text="Impedancia total",bg="#B0E4ED",font=("Open Sans", 10))
        self.ZTLabel.grid(row=0,column=0,sticky="w",padx=2,pady=1)

        self.ZTModo=ttk.Combobox(self.frameZT,values=self.modo,width=8,state="readonly",font=("Open Sans", 10))
        self.ZTModo.current(0)
        self.ZTModo.grid(row=0,column=1,padx=2,pady=1,sticky="w")    

        self.ZTLabel2=Label(self.frameZT,text="=",bg="#B0E4ED",font=("Open Sans", 10),anchor="w",justify="left")
        self.ZTLabel2.grid(row=0,column=2,padx=2,pady=1,sticky="w")

        #Entry de impedancia total
        self.ZTEntry=Entry(self.frameC,font=("Open Sans", 10))
        self.ZTEntry.grid(row=7,column=0,columnspan=2,padx=5,pady=2,sticky="we")
        self.ZTEntry.insert(0,self.ZT_Bin_STR)
        self.ZTEntry.config(state="readonly")

        self.ZTModo.bind("<<ComboboxSelected>>",lambda _:App._BinPol(self,frameA,self.ZTModo,self.ZTEntry,self.ZT_Bin_STR,self.ZT_Polar_STR))

        #Frame de corriente total (solo los label y el combobox)
        self.frameIT=Frame(self.frameC,bg="#B0E4ED")
        self.frameIT.grid(row=9,column=0,columnspan=2,sticky="we")

        self.ITLabel=Label(self.frameIT,text="Corriente total",bg="#B0E4ED",font=("Open Sans", 10))
        self.ITLabel.grid(row=0,column=0,sticky="w",padx=2,pady=1)

        self.ITModo=ttk.Combobox(self.frameIT,values=self.modo,width=8,state="readonly",font=("Open Sans", 10))
        self.ITModo.current(0)
        self.ITModo.grid(row=0,column=1,padx=2,pady=1,sticky="w")

        self.ITLabel2=Label(self.frameIT,text="=",bg="#B0E4ED",font=("Open Sans", 10),anchor="w",justify="left")
        self.ITLabel2.grid(row=0,column=2,padx=2,pady=1,sticky="w")

        #Entry de corriente total
        self.ITEntry=Entry(self.frameC,font=("Open Sans", 10))
        self.ITEntry.grid(row=10,column=0,columnspan=2,padx=5,pady=2,sticky="we")
        self.ITEntry.insert(0,self.IT_Bin_STR)
        self.ITEntry.config(state="readonly")

        self.ITModo.bind("<<ComboboxSelected>>",lambda _:App._BinPol(self,frameA,self.ITModo,self.ITEntry,self.IT_Bin_STR,self.IT_Polar_STR))

        #Texto de Labels de especial R/L/C
        if esParalelo==False:
            self.textoR="Tensión en R"
            self.textoL="Tensión en L"
            self.textoC="Tensión en C"
        else:
            self.textoR="Corriente en R"
            self.textoL="Corriente en L"
            self.textoC="Corriente en C"

        #Frame de tensión/corriente en R (Esp es de específico, son lo que cambia según el tipo de circuito seleccionado).
        self.frameEspR=Frame(self.frameC,bg="#B0E4ED")
        self.frameEspR.grid(row=12,column=0,columnspan=2,sticky="we")

        self.EspRLabel=Label(self.frameEspR,text=self.textoR,bg="#B0E4ED",font=("Open Sans", 10))
        self.EspRLabel.grid(row=0,column=0,sticky="w",padx=2,pady=1)

        self.EspRModo=ttk.Combobox(self.frameEspR,values=self.modo,width=8,state="readonly",font=("Open Sans", 10))
        self.EspRModo.current(0)
        self.EspRModo.grid(row=0,column=1,padx=2,pady=1,sticky="w")

        self.EspRLabel2=Label(self.frameEspR,text="=",bg="#B0E4ED",font=("Open Sans", 10),anchor="w",justify="left")
        self.EspRLabel2.grid(row=0,column=2,padx=2,pady=1,sticky="w")

        #Entry de tensión/corriente en R
        self.EspREntry=Entry(self.frameC,font=("Open Sans", 10))
        self.EspREntry.grid(row=13,column=0,columnspan=2,padx=5,pady=(5,10),sticky="we")
        self.EspREntry.insert(0,self.EspR_Bin_STR)
        self.EspREntry.config(state="readonly")

        self.EspRModo.bind("<<ComboboxSelected>>",lambda _:App._BinPol(self,frameA,self.EspRModo,self.EspREntry,self.EspR_Bin_STR,self.EspR_Polar_STR))

        #Frame de tensión/corriente en L
        self.frameEspL=Frame(self.frameC,bg="#B0E4ED")
        self.frameEspL.grid(row=14,column=0,columnspan=2,sticky="we")

        self.EspLLabel=Label(self.frameEspL,text=self.textoL,bg="#B0E4ED",font=("Open Sans", 10))
        self.EspLLabel.grid(row=0,column=0,sticky="w",padx=2,pady=1)

        self.EspLModo=ttk.Combobox(self.frameEspL,values=self.modo,width=8,state="readonly",font=("Open Sans", 10))
        self.EspLModo.current(0)
        self.EspLModo.grid(row=0,column=1,padx=2,pady=1,sticky="w")

        self.EspLLabel2=Label(self.frameEspL,text="=",bg="#B0E4ED",font=("Open Sans", 10),anchor="w",justify="left")
        self.EspLLabel2.grid(row=0,column=2,padx=2,pady=1,sticky="w")

        #Entry de tensión/corriente en L
        self.EspLEntry=Entry(self.frameC,font=("Open Sans", 10))
        self.EspLEntry.grid(row=15,column=0,columnspan=2,padx=5,pady=(5,10),sticky="we")
        self.EspLEntry.insert(0,self.EspL_Bin_STR)
        self.EspLEntry.config(state="readonly")

        self.EspLModo.bind("<<ComboboxSelected>>",lambda _:App._BinPol(self,frameA,self.EspLModo,self.EspLEntry,self.EspL_Bin_STR,self.EspL_Polar_STR))

        #Frame de tensión/corriente en C
        self.frameEspC=Frame(self.frameC,bg="#B0E4ED")
        self.frameEspC.grid(row=16,column=0,columnspan=2,sticky="we")

        self.EspCLabel=Label(self.frameEspC,text=self.textoC,bg="#B0E4ED",font=("Open Sans", 10))
        self.EspCLabel.grid(row=0,column=0,sticky="w",padx=2,pady=1)

        self.EspCModo=ttk.Combobox(self.frameEspC,values=self.modo,width=8,state="readonly",font=("Open Sans", 10))
        self.EspCModo.current(0)
        self.EspCModo.grid(row=0,column=1,padx=2,pady=1,sticky="w")

        self.EspCLabel2=Label(self.frameEspC,text="=",bg="#B0E4ED",font=("Open Sans", 10),anchor="w",justify="left")
        self.EspCLabel2.grid(row=0,column=2,padx=2,pady=1,sticky="w")

        #Entry de tensión/corriente en C
        self.EspCEntry=Entry(self.frameC,font=("Open Sans", 10))
        self.EspCEntry.grid(row=17,column=0,columnspan=2,padx=5,pady=(5,2),sticky="we")
        self.EspCEntry.insert(0,self.EspC_Bin_STR)
        self.EspCEntry.config(state="readonly")

        self.EspCModo.bind("<<ComboboxSelected>>",lambda _:App._BinPol(self,frameA,self.EspCModo,self.EspCEntry,self.EspC_Bin_STR,self.EspC_Polar_STR))
        
        #Sección potencias
        #Potencia aparente
        self.SLabel=Label(self.frameC,text="Potencia aparente (S) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.SLabel.grid(row=19,column=0,sticky="e",padx=2,pady=1)

        self.SEntry=Entry(self.frameC,width=28,font=("Open Sans", 10))
        self.SEntry.grid(row=19,column=1,padx=(2,5),pady=1)
        self.SEntry.insert(0,self.S_STR)
        self.SEntry.config(state="readonly")

        #Potencia activa
        self.PLabel=Label(self.frameC,text="Potencia activa (P) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.PLabel.grid(row=20,column=0,sticky="e",padx=2,pady=1)

        self.PEntry=Entry(self.frameC,width=28,font=("Open Sans", 10))
        self.PEntry.grid(row=20,column=1,padx=(2,5),pady=1)
        self.PEntry.insert(0,self.P_STR)
        self.PEntry.config(state="readonly")

        #Potencia reactiva
        self.QLabel=Label(self.frameC,text="Potencia reactiva (Q) =",bg="#B0E4ED",font=("Open Sans", 10))
        self.QLabel.grid(row=21,column=0,sticky="e",padx=2,pady=1)

        self.QEntry=Entry(self.frameC,width=28,font=("Open Sans", 10))
        self.QEntry.grid(row=21,column=1,padx=(2,5),pady=1)
        self.QEntry.insert(0,self.Q_STR)
        self.QEntry.config(state="readonly")

        #Factor de potencia
        self.FPLabel=Label(self.frameC,text="Factor de potencia =",bg="#B0E4ED",font=("Open Sans", 10))
        self.FPLabel.grid(row=22,column=0,sticky="e",padx=2,pady=1)

        self.FPEntry=Entry(self.frameC,width=28,font=("Open Sans", 10))
        self.FPEntry.grid(row=22,column=1,padx=(2,5),pady=1)
        self.FPEntry.insert(0,self.FP_STR)
        self.FPEntry.config(state="readonly")

    def ResultadosTexto(self): #Genera un string con todos los resultados (para copiarlos o guardarlos).
        if esParalelo==False:
            titulo="ANÁLISIS DE CIRCUITO RLC SERIE"
        else:
            titulo="ANÁLISIS DE CIRCUITO RLC PARALELO"
        
        #\r + \n (implícito en este tipo de string) = \r\n (salto de línea que va con el notepad de Windows).
        self.textoRes=titulo + """\r
Datos ingresados:\r
Tensión (V) = """ + self.VIN + """\r
Frecuencia (f) = """ + self.fIN + """\r
Resistencia (R) = """ + self.RIN + """\r
Inductancia (L) = """ + self.LIN + """\r
Capacitancia (C) = """ + self.CIN + """\r
\r
Resultados:\r
Frecuencia de resonancia = """ + self.resonancia_STR + """\r
Reactancia inductiva de la bobina (L) =""" + self.XL_STR + """\r
Reactancia capacitiva del capacitor (C) = """ + self.XC_STR + """\r
\r
Impedancia total (binómica) = """ + self.ZT_Bin_STR + """\r
Impedancia total (polar) = """ + self.ZT_Polar_STR + """\r
\r
Corriente total (binómica) = """ + self.IT_Bin_STR + """\r
Corriente total (polar) = """ + self.IT_Polar_STR + """\r
\r
""" + self.textoR + " (binómica) = " + self.EspR_Bin_STR + """\r
""" + self.textoR + " (polar) = " + self.EspR_Polar_STR + """\r
""" + self.textoL + " (binómica) = " + self.EspL_Bin_STR + """\r
""" + self.textoL + " (polar) = " + self.EspL_Polar_STR + """\r
""" + self.textoC + " (binómica) = " + self.EspC_Bin_STR + """\r
""" + self.textoC + " (polar) = " + self.EspC_Polar_STR + """\r
\r
Potencia aparente (S) = """ + self.S_STR + """\r
Potencia activa (P) = """ + self.P_STR + """\r
Potencia reactiva (Q) = """ + self.Q_STR + """\r
Factor de potencia = """ + self.FP_STR
    
    def CopiarResultados(self):     #Copia los resultados al portapapeles
        App.ResultadosTexto(self)
        clipboard.copy(self.textoRes)
        messagebox.showinfo(title="Resultados",message="Resultados copiados con éxito.")

    def GuardarResultados(self):    #Guarda los resultados en un .txt
        App.ResultadosTexto(self)
        guardarArchivo=filedialog.asksaveasfilename(initialdir="/",title="Guardar resultados",filetypes=(("Documentos de texto (.txt)","*.txt"),("Todos los archivos","*.*")),defaultextension=".txt")
        if guardarArchivo: #Si se selecciono un archivo, entonces se cumple. Si se da a cancelar en la ventana de guardar, entonces no pasa nada (y no tira error).
            with open(str(guardarArchivo),"w",encoding="utf-8") as datos: #La codificación "utf-8" permite utilizar el Ω y el φ.
                datos.write(self.textoRes)
                datos.close()
            messagebox.showinfo(title="Resultados",message="Resultados guardados con éxito.")

    def _BinPol(self,frame,Combobox,Entry,Bin,Pol):  #Función asociada a todas las combobox de binómica/polar.
        c=Combobox.get()
        Entry.config(state="normal")
        Entry.delete(0,END)
        if c=="binómica":
            Entry.insert(0,Bin)
        else:
            Entry.insert(0,Pol)
        Entry.config(state="readonly")
        frame.focus_set()

    def ReemplazoComa(self,string): #Permite utilizar la coma como separador decimal.
        if string.count(",")==1 and string.endswith(",")==False:
            res=string.replace(",",".")
        else:
            res=string
        return res

    def ConvertirFloat(self,string): #Evalúa y convierte a float según si es posible o no.
        #Este if evita que el programa acepte "inf", "NaN" y exponentes (E).
        if "e" in string or "E" in string or "a" in string or "A" in string or "i" in string or "I" in string:
            res=0
            correcto=False
        else:
            try:
                res=float(string)
                if res<0:       #Evita que se ingresen valores negativos.
                    correcto=False
                else:
                    correcto=True
            except ValueError:
                res=0
                correcto=False
        return res,correcto

    def Multiplo(self,mult):    #Asigna un valor numérico a cada opción del Combobox.
        if mult=="G":
            res=1e9
        elif mult=="M":
            res=1e6
        elif mult=="K":
            res=1e3
        elif mult=="":
            res=1
        elif mult=="m":
            res=1e-3
        elif mult=="μ":
            res=1e-6
        elif mult=="n":
            res=1e-9
        else:
            res=1e-12
        return res

    def LabelX(self,correcto,label):    #Activa y desactiva los label de dato erróneo.
        if correcto==False:
            label.config(text="X",bg="red")
        else:
            label.config(text="",bg="#B0E4ED")

    def FormatoDatos(self,n): #Redondeo de enteros y coma decimal de los datos ingresados.
        if n%1==0:
            res=str(int(n))
        elif SeparadorDecimal==1:   #La configuración de separador decimal de resultados también afecta a estos datos.
            res=str(n).replace(".",",")
        else:
            res=str(n)
        return res
    
    def Redondeo(self,Valor):
        if Valor==0:       #Corrección gráfica de "menos cero".
            Res=0
        else:
            Valor=round(Valor,Red) #Red son los dígitos de redondeo (por defecto 4).
            if Valor%1==0:
                Valor=int(Valor)
            Res=Valor
        return Res

    def strRes(self,Valor): #Pasa los resultados a string.
        Res=str(Valor)
        if SeparadorDecimal==1:
            Res=Res.replace(".",",") #Coma decimal en resultados.
        return Res

    def FormatoRes(self,Valor,Modulo=False):  #Formato de resultados.
        if(Modulo):
            Valor=abs(Valor)
        
        if Valor>=1e9 or Valor<=-1e9:
            Valor=App.Redondeo(self,Valor/1e9)
            Res=App.strRes(self,Valor) + " G"

        elif Valor>=1e6 or Valor<=-1e6:
            Valor=App.Redondeo(self,Valor/1e6)
            if Valor==1e3:     # Corrige error. Si el resultado del redondeo da 1000, entonces pasa 
                Res="1 G"      # a valer 1 y se le asigna el múltiplo superior.
            elif Valor==-1e3:
                Res="-1 G"
            else:
                Res=App.strRes(self,Valor) + " M"

        elif Valor>=1e3 or Valor<=-1e3:
            Valor=App.Redondeo(self,Valor/1e3)
            if Valor==1e3:
                Res="1 M"
            elif Valor==-1e3:
                Res="-1 M"
            else:
                Res=App.strRes(self,Valor) + " K"

        elif Valor>=1 or Valor<=-1 or Valor==0:
            Valor=App.Redondeo(self,Valor)
            if Valor==1e3:
                Res="1 K"
            elif Valor==-1e3:
                Res="-1 K"
            else:
                Res=App.strRes(self,Valor) + " "

        elif Valor>=1e-3 or Valor<=-1e-3:
            Valor=App.Redondeo(self,Valor*1e3)
            if Valor==1e3:
                Res="1 "
            elif Valor==-1e3:
                Res="-1 "
            else:
                Res=App.strRes(self,Valor) + " m"

        elif Valor>=1e-6 or Valor<=-1e-6:
            Valor=App.Redondeo(self,Valor*1e6)
            if Valor==1e3:
                Res="1 m"
            elif Valor==-1e3:
                Res="-1 m"
            else:
                Res=App.strRes(self,Valor) + " μ"

        elif Valor>=1e-9 or Valor<=-1e-9:
            Valor=App.Redondeo(self,Valor*1e9)
            if Valor==1e3:
                Res="1 μ"
            elif Valor==-1e3:
                Res="-1 μ"
            else:
                Res=App.strRes(self,Valor) + " n"

        else:
            Valor=App.Redondeo(self,Valor*1e12)
            if Valor==1e3:
                Res="1 n"
            elif Valor==-1e3:
                Res="-1 n"
            else:
                Res=App.strRes(self,Valor) + " p"

        return Res

    def FormatoAngulos(self,Angulo): #Formato de los ángulos de los resultados.
        if SignoAngulos==1 and Angulo<0: #Cambia el valor si la configuración está en "solo positivos" (Ejemplo: pasa -40° a 320°).
            Angulo=Angulo+360

        if ConfigAngulos==1:    #Pasa el ángulo a grados, minutos y segundos.
            if Angulo<0:
                signo="-"
            else:
                signo=""
    
            Angulo=abs(Angulo)
            grados=Angulo//1
            minutos=(Angulo%1)*60
            segundos=(minutos%1)*60
            minutos=minutos//1

            if minutos!=0:
                minutos_STR=App.strRes(self,App.Redondeo(self,minutos)) + "′ "
            else:
                minutos_STR=""

            if segundos!=0:
                segundos_STR=App.strRes(self,App.Redondeo(self,segundos)) + "″"
            else:
                segundos_STR=""

            res=signo + App.strRes(self,App.Redondeo(self,grados)) + "° " + minutos_STR + segundos_STR
        else:
            res=App.strRes(self,App.Redondeo(self,Angulo)) + "°"
        return res

    def Inicio(self,frameA):                   #Regresa al menú inicio.
        mensajeConfirmar=messagebox.askyesno(title="Volver al inicio",message="¿Desea volver al menú principal?")
        if(mensajeConfirmar):
            frameA.destroy()
            App.Actual=Inicio(self.master)

class Inicio:
    def __init__(self,master):
        self.master=master

        self.frameA=Frame(self.master,bg="#B0E4ED")
        self.frameA.pack(fill="both",expand=True)

        self.saludoLabel=Label(self.frameA,text="Bienvenido/a a la",bg="#B0E4ED",font=("Open Sans Bold", 18))
        self.saludoLabel.grid(row=0,column=0,padx=10,pady=2,columnspan=2)

        self.tituloLabel=Label(self.frameA,text="Calculadora RLC",bg="#B0E4ED",font=("Open Sans ExtraBold", 36))
        self.tituloLabel.grid(row=1,column=0,padx=10,columnspan=2)

        self.versionLabel=Label(self.frameA,text="versión Beta 1.0",bg="#B0E4ED",font=("Open Sans Bold", 16),anchor="n")
        self.versionLabel.grid(row=2,column=0,columnspan=2)

        self.imagenLabel=Label(self.frameA,image=imagenLogo,bg="#B0E4ED")
        self.imagenLabel.grid(row=3,column=0,pady=20,columnspan=2)

        self.botonSerie=Button(self.frameA,text="Analizar RLC serie",bd=3,width=34,font=("Open Sans", 11),command=lambda:self.Serie())
        self.botonSerie.config(activebackground="lightblue")
        self.botonSerie.grid(row=4,column=0,padx=10,pady=10,columnspan=2)

        self.botonParalelo=Button(self.frameA,text="Analizar RLC paralelo",bd=3,width=34,font=("Open Sans", 11),command=lambda:self.Paralelo())
        self.botonParalelo.config(activebackground="lightblue")
        self.botonParalelo.grid(row=5,column=0,padx=10,pady=10,columnspan=2)

        self.botonConfig=Button(self.frameA,text="Configuración",bd=3,width=34,font=("Open Sans", 11),command=lambda:self.Configuracion())
        self.botonConfig.config(activebackground="lightblue")
        self.botonConfig.grid(row=6,column=0,padx=10,pady=10,columnspan=2)

        self.botonAcerca=Button(self.frameA,text="Acerca de",bd=3,width=15,font=("Open Sans", 11),command=lambda:self.AcercaDe())
        self.botonAcerca.config(activebackground="lightblue")
        self.botonAcerca.grid(row=7,column=0,padx=10,pady=10,sticky="e")

        self.botonSalir=Button(self.frameA,text="Salir",bd=3,width=15,font=("Open Sans", 11),command=lambda:raiz.destroy())
        self.botonSalir.config(activebackground="lightblue")
        self.botonSalir.grid(row=7,column=1,padx=10,pady=10,sticky="w")
    
    def Serie(self):
        self.frameA.destroy()
        App.Actual=Serie(self.master)

    def Paralelo(self):
        self.frameA.destroy()
        App.Actual=Paralelo(self.master)
        
    def Configuracion(self):
        self.frameA.destroy()
        App.Actual=Configuracion(self.master)

    def AcercaDe(self):
        self.frameA.destroy()
        App.Actual=AcercaDe(self.master)

class Serie:
    def __init__(self,master):
        self.master=master

        self.frameA=Frame(self.master,bg="#B0E4ED")
        self.frameA.pack(fill="both",expand=True)

        self.tituloLabel=Label(self.frameA,text="Análisis de circuito RLC serie",bg="#B0E4ED",anchor="w",justify="left",font=("Open Sans ExtraBold", 20))
        self.tituloLabel.grid(row=0,column=0,columnspan=2,padx=10,pady=5,sticky="w")

        App.IngresoDatos(self,self.frameA)

        self.imagenCircuitoLabel=Label(self.frameA,image=imagenSerie,bg="#B0E4ED")
        self.imagenCircuitoLabel.grid(row=1,column=1,padx=(5,20),pady=(5,20))

        self.frameBotones=Frame(self.frameA,bg="#B0E4ED")
        self.frameBotones.grid(row=2,column=0,columnspan=2,pady=10)

        self.botonCalcular=Button(self.frameBotones,text="Calcular",font=("Open Sans", 11),width=12,activebackground="lightblue",command=lambda:App.Calcular(self,self.frameA))
        self.botonCalcular.grid(row=0,column=0,padx=15)

        self.botonInicio=Button(self.frameBotones,text="Volver al inicio",font=("Open Sans", 11),width=12,activebackground="lightblue",command=lambda:App.Inicio(self,self.frameA))
        self.botonInicio.grid(row=0,column=1,padx=15)

class ResultadosSerie:
    def __init__(self,master):
        self.master=master

        self.frameA=Frame(self.master,bg="#B0E4ED")
        self.frameA.pack(fill="both",expand=True)

        self.tituloLabel=Label(self.frameA,text="Análisis de circuito RLC serie",bg="#B0E4ED",anchor="w",justify="left",font=("Open Sans ExtraBold", 20))
        self.tituloLabel.grid(row=0,column=0,columnspan=2,padx=10,pady=5,sticky="w")

        App.Resultados(self,self.frameA)

        self.imagenCircuitoLabel=Label(self.frameA,image=imagenSerie,bg="#B0E4ED")
        self.imagenCircuitoLabel.grid(row=2,column=0,pady=20)

        self.frameBotones=Frame(self.frameA,bg="#B0E4ED")
        self.frameBotones.grid(row=3,column=0,columnspan=2,pady=(20,10))

        self.botonCopiar=Button(self.frameBotones,text="Copiar resultados al portapapeles",font=("Open Sans", 11),activebackground="lightblue",command=lambda:App.CopiarResultados(self))
        self.botonCopiar.grid(row=0,column=0,padx=20)

        self.botonGuardar=Button(self.frameBotones,text="Guardar resultados",font=("Open Sans", 11),activebackground="lightblue",command=lambda:App.GuardarResultados(self))
        self.botonGuardar.grid(row=0,column=1,padx=20)

        self.botonInicio=Button(self.frameBotones,text="Volver al inicio",font=("Open Sans", 11),activebackground="lightblue",command=lambda:App.Inicio(self,self.frameA))
        self.botonInicio.grid(row=0,column=2,padx=20)

class Paralelo:
    def __init__(self,master):
        self.master=master

        self.frameA=Frame(self.master,bg="#B0E4ED")
        self.frameA.pack(fill="both",expand=True)

        self.tituloLabel=Label(self.frameA,text="Análisis de circuito RLC paralelo",bg="#B0E4ED",anchor="w",justify="left",font=("Open Sans ExtraBold", 20))
        self.tituloLabel.grid(row=0,column=0,columnspan=2,padx=10,pady=5,sticky="w")

        App.IngresoDatos(self,self.frameA)

        self.imagenCircuitoLabel=Label(self.frameA,image=imagenParalelo,bg="#B0E4ED",anchor="nw")
        self.imagenCircuitoLabel.grid(row=1,column=1,padx=(5,20),pady=20,sticky="nw")

        self.frameBotones=Frame(self.frameA,bg="#B0E4ED")
        self.frameBotones.grid(row=2,column=0,columnspan=2,pady=10)

        self.botonCalcular=Button(self.frameBotones,text="Calcular",font=("Open Sans", 11),width=12,activebackground="lightblue",command=lambda:App.Calcular(self,self.frameA))
        self.botonCalcular.grid(row=0,column=0,padx=15)

        self.botonInicio=Button(self.frameBotones,text="Volver al inicio",font=("Open Sans", 11),width=12,activebackground="lightblue",command=lambda:App.Inicio(self,self.frameA))
        self.botonInicio.grid(row=0,column=1,padx=15)

class ResultadosParalelo:
    def __init__(self,master):
        self.master=master

        self.frameA=Frame(self.master,bg="#B0E4ED")
        self.frameA.pack(fill="both",expand=True)

        self.tituloLabel=Label(self.frameA,text="Análisis de circuito RLC paralelo",bg="#B0E4ED",anchor="w",justify="left",font=("Open Sans ExtraBold", 20))
        self.tituloLabel.grid(row=0,column=0,columnspan=2,padx=10,pady=5,sticky="w")

        App.Resultados(self,self.frameA)

        self.imagenCircuitoLabel=Label(self.frameA,image=imagenParalelo,bg="#B0E4ED")
        self.imagenCircuitoLabel.grid(row=1,column=0,rowspan=2,padx=5)  #Se superpone con la row=1 del frameB, pero se centra y no pasa nada.

        self.frameBotones=Frame(self.frameA,bg="#B0E4ED")
        self.frameBotones.grid(row=3,column=0,columnspan=2,pady=(20,10))

        self.botonCopiar=Button(self.frameBotones,text="Copiar resultados al portapapeles",activebackground="lightblue",font=("Open Sans", 11),command=lambda:App.CopiarResultados(self))
        self.botonCopiar.grid(row=0,column=0,padx=20)

        self.botonGuardar=Button(self.frameBotones,text="Guardar resultados",font=("Open Sans", 11),activebackground="lightblue",command=lambda:App.GuardarResultados(self))
        self.botonGuardar.grid(row=0,column=1,padx=20)

        self.botonInicio=Button(self.frameBotones,text="Volver al inicio",font=("Open Sans", 11),activebackground="lightblue",command=lambda:App.Inicio(self,self.frameA))
        self.botonInicio.grid(row=0,column=2,padx=20)

class Configuracion:
    def __init__(self,master):
        self.master=master

        self.decimal=("punto","coma") #Opciones de separador decimal
        self.grados=("grados con decimales", "grados, minutos y segundos") #Opciones de configuración de grados
        self.signoGrados=("positivos y negativos","solo positivos")

        self.frameA=Frame(self.master,bg="#B0E4ED")
        self.frameA.pack(fill="both",expand=True)

        self.tituloLabel=Label(self.frameA,text="Configuración",bg="#B0E4ED",anchor="w",justify="left",font=("Open Sans ExtraBold", 20))
        self.tituloLabel.grid(row=0,column=0,columnspan=2,padx=10,pady=5,sticky="w")

        #Bucle de separadores.
        self.separadores=(3,6)
        
        for pos in self.separadores:
            self.barra=ttk.Separator(self.frameA,style="TSeparator")
            self.barra.grid(row=pos,column=0,columnspan=2,pady=6,padx=8,sticky="we")

        #Configuración de redondeo.
        self.redondeoLabel=Label(self.frameA,text="Cantidad de dígitos del redondeo =",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.redondeoLabel.grid(row=1,column=0,sticky="e",padx=2,pady=(1,0))

        self.redondeoLabel2=Label(self.frameA,text="(Por defecto = 4)    ",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.redondeoLabel2.grid(row=2,column=0,sticky="e",padx=2,pady=(0,1))

        self.redondeo=StringVar(value=Red)
        self.redondeoSpinbox=Spinbox(self.frameA,from_=0,to=15,textvariable=self.redondeo,font=("Open Sans", 11),width=3,justify="center",state="readonly",wrap=True)
        self.redondeoSpinbox.grid(row=1,column=1,sticky="w",padx=2,pady=1)

        #Configuración de separador decimal.
        self.decimalLabel=Label(self.frameA,text="Separador decimal de los resultados =",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.decimalLabel.grid(row=4,column=0,sticky="e",padx=2,pady=(1,0))

        self.decimalLabel2=Label(self.frameA,text="(Por defecto = coma)    ",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.decimalLabel2.grid(row=5,column=0,sticky="e",padx=2,pady=(0,1))

        self.decimalCombobox=ttk.Combobox(self.frameA,values=self.decimal,width=7,state="readonly",font=("Open Sans", 10))
        self.decimalCombobox.current(SeparadorDecimal)
        self.decimalCombobox.grid(row=4,column=1,sticky="w",padx=2,pady=1)
        self.decimalCombobox.bind("<<ComboboxSelected>>",lambda _:self.frameA.focus_set())

        #Configuración de grados
        self.gradosLabel=Label(self.frameA,text="Configuración de los ángulos =",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.gradosLabel.grid(row=7,column=0,sticky="e",padx=2,pady=(1,0))

        self.gradosLabel2=Label(self.frameA,text="(Por defecto = grados con decimales)    ",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.gradosLabel2.grid(row=8,column=0,sticky="e",padx=(10,2),pady=(0,1))

        self.gradosCombobox=ttk.Combobox(self.frameA,values=self.grados,width=24,state="readonly",font=("Open Sans", 10))
        self.gradosCombobox.current(ConfigAngulos)
        self.gradosCombobox.grid(row=7,column=1,sticky="w",padx=(2,10),pady=1)
        self.gradosCombobox.bind("<<ComboboxSelected>>",lambda _:self.frameA.focus_set())

        self.signoGradosLabel=Label(self.frameA,text="Signo de los ángulos =",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.signoGradosLabel.grid(row=9,column=0,sticky="e",padx=2,pady=(1,0))

        self.signoGradosLabel2=Label(self.frameA,text="(Por defecto = positivos y negativos)    ",bg="#B0E4ED",font=("Open Sans", 10),anchor="e",justify="right")
        self.signoGradosLabel2.grid(row=10,column=0,sticky="e",padx=2,pady=(0,1))

        self.signoGradosCombobox=ttk.Combobox(self.frameA,values=self.signoGrados,width=18,state="readonly",font=("Open Sans", 10))
        self.signoGradosCombobox.current(SignoAngulos)
        self.signoGradosCombobox.grid(row=9,column=1,sticky="w",padx=2,pady=1)
        self.signoGradosCombobox.bind("<<ComboboxSelected>>",lambda _:self.frameA.focus_set())

        self.frameBotones=Frame(self.frameA,bg="#B0E4ED")
        self.frameBotones.grid(row=11,column=0,columnspan=2,pady=(20,10))

        self.botonGuardarCambios=Button(self.frameBotones,text="Guardar cambios y volver",font=("Open Sans", 11),activebackground="lightblue",command=self.GuardarCambios)
        self.botonGuardarCambios.grid(row=0,column=0,padx=15)

        self.botonInicio=Button(self.frameBotones,text="Volver al inicio sin guardar",font=("Open Sans", 11),activebackground="lightblue",command=lambda:App.Inicio(self,self.frameA))
        self.botonInicio.grid(row=0,column=1,padx=15)
    
    def GuardarCambios(self):
        global Red,SeparadorDecimal,ConfigAngulos,SignoAngulos
        Red=int(self.redondeoSpinbox.get())
        
        if self.decimalCombobox.get()=="punto":
            SeparadorDecimal=0
        else:
            SeparadorDecimal=1
        
        if self.gradosCombobox.get()=="grados con decimales":
            ConfigAngulos=0
        else:
            ConfigAngulos=1
        
        if self.signoGradosCombobox.get()=="positivos y negativos":
            SignoAngulos=0
        else:
            SignoAngulos=1

        messagebox.showinfo(title="Guardar cambios",message="Cambios guardados con éxito.")
        self.frameA.destroy()
        App.Actual=Inicio(self.master)

class AcercaDe:
    def __init__(self,master):
        self.master=master

        self.frameA=Frame(self.master,bg="#B0E4ED")
        self.frameA.pack(fill="both",expand=True)

        #Bucle de separadores.
        self.separadores=(2,5)
        
        for pos in self.separadores:
            self.barra=ttk.Separator(self.frameA,style="TSeparator")
            self.barra.grid(row=pos,column=0,columnspan=2,pady=6,padx=10,sticky="we")

        self.tituloLabel=Label(self.frameA,text="Acerca de la Calculadora RLC",bg="#B0E4ED",font=("Open Sans ExtraBold", 18))
        self.tituloLabel.grid(row=0,column=0,columnspan=2,padx=10,pady=5)

        self.imagenLabel=Label(self.frameA,image=imagenLogoChico,bg="#B0E4ED")
        self.imagenLabel.grid(row=1,column=0,padx=(10,5),pady=5)
        
        self.descripcionLabel=Label(self.frameA,bg="#B0E4ED",font=("Open Sans", 10),justify="left")
        self.descripcionLabel.config(text="""Versión Beta 1.0
Este programa fue realizado en 2020,
programado en Python con el módulo TKinter.""")
        self.descripcionLabel.grid(row=1,column=1,padx=10,pady=1,sticky="w")

        self.contactoLabel=Label(self.frameA,bg="#B0E4ED",font=("Open Sans", 10))
        self.contactoLabel.config(text="""Si quiere avisar acerca de un problema, así
como si tiene dudas, consultas o sugerencias,
puede comunicarse a la siguiente dirección.""")
        self.contactoLabel.grid(row=3,column=0,columnspan=2,padx=10,pady=1)

        self.correoLabel=Label(self.frameA,bg="#B0E4ED",font=("Open Sans", 11),text="calculadora.rlc@gmail.com")
        self.correoLabel.grid(row=4,column=0,columnspan=2,padx=10,pady=1)

        self.graciasLabel=Label(self.frameA,bg="#B0E4ED",font=("Open Sans", 11),text="Muchas gracias por utilizar la Calculadora RLC.")
        self.graciasLabel.grid(row=6,column=0,columnspan=2,padx=10,pady=5)

        self.botonInicio=Button(self.frameA,text="Volver al inicio",font=("Open Sans", 11),activebackground="lightblue",command=self.Inicio)
        self.botonInicio.grid(row=7,column=0,columnspan=2,padx=10,pady=10)

    def Inicio(self):
        self.frameA.destroy()
        App.Actual=Inicio(self.master)


raiz=Tk()

main=App(raiz)