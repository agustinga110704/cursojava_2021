/* Con relaci�n al punto 16 mostrar la suma de los valores pares.
 * a. Con if */

package modulo4;
import java.util.Scanner;

public class Ejercicio17a {

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un n�mero ---> ");
		int n = ingreso.nextInt();
		
		System.out.println("Tabla de multiplicar del " + n + ":");
		
		int producto;
		int suma=0;
		for(int i=0;i<11;i++){
			producto = n*i;
			if(producto%2==0){
				suma+=producto;
			}
			
			System.out.println(n + " x " + i + " = " + producto);
		}
		System.out.println("La suma de los valores pares es " + suma);
		
		ingreso.close();

	}

}
