/* Realizar un programa que permita mostrar en pantalla la tabla de multiplicar 
 * de un valor ingresado a trav�s de una variable. */

package modulo4;
import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un n�mero ---> ");
		int n = ingreso.nextInt();
		
		System.out.println("Tabla de multiplicar del " + n + ":");
		
		for(int i=0;i<11;i++){
			System.out.println(n + " x " + i + " = " + n*i);
		}
		
		ingreso.close();
	}

}
