/* Realizar un programa que permita identificar con una variable de tipo entero el puesto 
 * que ocupa  un torneo, existen 3 posiciones que son premiadas para los cuales deber� 
 * mostrar el siguiente mensaje en pantalla:
 *  a. El primero obtiene la medalla de oro.
 *  b. El segundo obtiene la medalla de plata.
 *  c. Y el tercero obtiene la medalla de bronce.
 *  d. Y para el resto siga participando. */

package modulo4;
import java.util.Scanner;

public class Ejercicio05 {

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Introduzca el puesto del competidor ---> ");
		int pos = ingreso.nextInt();
		
		if(pos==1){
			System.out.println("El competidor obtiene la medalla de oro.");
		}
		else if(pos==2){
			System.out.println("El competidor obtiene la medalla de plata.");
		}
		else if(pos==3){
			System.out.println("El competidor obtiene la medalla de bronce.");
		}
		else{
			System.out.println("Siga participando.");
		}
		
		ingreso.close();
	}

}
