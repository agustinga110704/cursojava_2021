// Elaborar un sistema que permita realizar el ejercicio numero 3 a utilizando esta vez la sentencia switch.

package modulo4;
import java.util.Scanner;

public class Ejercicio13 {
	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un mes ---> ");
		String mes=ingreso.nextLine();
		
		switch(mes){
			case "Enero":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "enero":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "Febrero":
				System.out.println(mes + " tiene 28 d�as.");
				break;
			case "febrero":
				System.out.println(mes + " tiene 28 d�as.");
				break;
			case "Marzo":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "marzo":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "Abril":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "abril":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "Mayo":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "mayo":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "Junio":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "junio":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "Julio":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "julio":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "Agosto":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "agosto":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "Septiembre":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "septiembre":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "Setiembre":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "setiembre":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "Octubre":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "octubre":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "Noviembre":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "noviembre":
				System.out.println(mes + " tiene 30 d�as.");
				break;
			case "Diciembre":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			case "diciembre":
				System.out.println(mes + " tiene 31 d�as.");
				break;
			default:
				System.out.println(mes + " no es un mes.");
		}
		
		ingreso.close();
	}

}