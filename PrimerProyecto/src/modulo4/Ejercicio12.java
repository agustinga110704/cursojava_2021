/* Realizar un sistema que permita asignar un numero dentro de una variable de tipo int y sabiendo que
 * a. La primer docena va de 1 a 12
 * b. La segunda docena va de 13 a 24
 * c. La tercer docena va de 25 a 36
 * Determinar a que docena pertenece el numero y si excediera estos limites informar
 * el a trav�s del mensaje �el numero xxx esta fuera de rango�
 * Nota realizarlo con and y con or. */

package modulo4;
import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un n�mero ---> ");
		int num = ingreso.nextInt();
		
		if(num>0 && num<13){
			System.out.println("El n�mero " + num + " pertenece a la primera docena.");
		}
		else if(num>12 && num<25){
			System.out.println("El n�mero " + num + " pertenece a la segunda docena.");
		}
		else if(num>24 && num<37){
			System.out.println("El n�mero " + num + " pertenece a la tercera docena.");
		}
		else{
			System.out.println("El n�mero " + num + " est� fuera de rango.");
		}
		
		ingreso.close();
	}

}
