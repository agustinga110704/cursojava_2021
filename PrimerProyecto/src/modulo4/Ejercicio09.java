// Realizar el ejercicio 8 utilizando la sentencia and para resolver el problema.

package modulo4;

import java.util.Scanner;

public class Ejercicio09 {

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.println("�Piedra (0), papel (1) o tijera (2)!");
		System.out.print("Ingrese la jugada del jugador 1 ---> ");
		int comp1 = ingreso.nextInt();
		
		System.out.print("Ingrese la jugada del jugador 2 ---> ");
		int comp2 = ingreso.nextInt();
		
		if(comp1<0 || comp1>2 || comp2<0 || comp2>2){
			System.out.println("ERROR: Jugada incorrecta.");
		}
		else if(comp1==comp2){
			System.out.println("�Empate!");
		}
		else{
			if(comp1==0 && comp2==1){
				System.out.println("�Gana el jugador 2!");
			}
			else if(comp1==0 && comp2==2){
				System.out.println("�Gana el jugador 1!");
			}
			else if(comp1==1 && comp2==0){
				System.out.println("�Gana el jugador 1!");
			}
			else if(comp1==1 && comp2==2){
				System.out.println("�Gana el jugador 2!");
			}
			else if(comp1==2 && comp2==0){
				System.out.println("�Gana el jugador 2!");
			}
			else{
				System.out.println("�Gana el jugador 1!");
			}
			
		}
		
		ingreso.close();

	}

}
