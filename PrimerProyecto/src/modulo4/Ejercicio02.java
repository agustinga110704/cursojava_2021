/* Realizar un programa que permita identificar si un n�mero es par o impar,
 * el mismo deber� estar guardado en una variable de tipo int. */

package modulo4;
import java.util.Scanner;

public class Ejercicio02 {
	public static void main (String[] args){
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un n�mero ---> ");
		int numero=ingreso.nextInt();
		
		if(numero%2==0){
			System.out.println("El n�mero " + numero + " es par.");
		}
		else{
			System.out.println("El n�mero " + numero + " es impar.");
		}
		
		ingreso.close();
	}
}
