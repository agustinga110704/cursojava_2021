/* Realizar un programa que permita asignarle a una variable de tipo 
 * char un caracter, luego deber� determinar si este caracter es una vocal o no,
 * realizando esta l�gica a trav�s de la uni�n l�gica or, ya que las vocales son 
 * �a�, �e�, �i�, �o�, �u� */

package modulo4;
import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		Scanner ingreso = new Scanner(System.in);
		
		System.out.print("Ingrese un car�cter ---> ");
		char letra = ingreso.next().charAt(0);
		
		if(letra=='a' || letra=='A' || letra=='e' || letra=='E' || letra=='i' ||
				letra=='I' || letra=='o' || letra=='O' || letra=='u' || letra=='U'){
			System.out.println("El car�cter " + letra + " es una vocal.");
		}
		else{
			System.out.println("El car�cter " + letra + " no es una vocal.");
		}
			
		ingreso.close();
	}

}
