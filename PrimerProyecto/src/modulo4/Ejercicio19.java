// Realizar un ciclo while que muestre 10 n�meros al azar informando su suma y su promedio.

package modulo4;

public class Ejercicio19 {

	public static void main(String[] args) {
		System.out.println("N�meros al azar:");
		
		int numero;
		double promedio;
		int suma=0;
		int c=1;
		
		while(c<11){
			c++;
			numero=(int)(Math.random()*100);
			suma+=numero;
			System.out.println(numero);
		}
		
		promedio=(double)suma/10;
		
		System.out.println("\nSuma = " + suma);
		System.out.println("Promedio = " + promedio);
	}

}
