// Realizar dos ciclos for anidados de manera de mostrar todas las tablas.

package modulo4;

public class Ejercicio18 {

	public static void main(String[] args) {
		System.out.println("Tablas de multiplicar del 0 al 10:");
		
		for(int n=0;n<11;n++){
			System.out.println("\nTabla del " + n + ":");
			for(int i=0;i<11;i++){
				System.out.println(n + " x " + i + " = " + n*i);
			}
		}

	}

}
