/* 20.	Realizar un ciclo do while que muestre 10 n�meros al azar (Math.random() )
 * informando el m�ximo y el m�nimo de ellos. */

package modulo4;

public class Ejercicio20 {

	public static void main(String[] args) {
		System.out.println("N�meros al azar:");
		
		int numero;
		int minimo=0;
		int maximo=0;
		int c=1;
		
		do{
			numero=(int)(Math.random()*100);
			if(c==1){
				minimo=numero;
				maximo=numero;
			}
			else if(numero>maximo){
				maximo=numero;
			}
			else if(numero<minimo){
				minimo=numero;
			}
			
			System.out.println(numero);
			c++;	
		} while(c<11);
		
		System.out.println("\nN�mero m�ximo = " + maximo);
		System.out.println("N�mero m�nimo = " + minimo);
	}

}
