package modulo1;

public class Ejercicio3 {

	public static void main(String[] args) {
		System.out.println("Tecla de escape\t\tSignificado\n");
		System.out.println("\\n\t\t\tSignifica nueva l�nea.");
		System.out.println("\\t\t\t\tSignifica un TAB de espacio.");
		System.out.println("\\\"\t\t\tEs para poner \" (comillas dobles) dentro del texto,");
		System.out.println("\t\t\tpor ejemplo: \"Belencita.\"");
		System.out.println("\\\\\t\t\tSe utiliza para escribir la \\ dentro del texto, por ejemplo: \\algo\\.");
		System.out.println("\\\'\t\t\tSe utiliza para las \' (comillas simples) para escribir, por ejemplo: \'Princesita\'.");

	}

}
