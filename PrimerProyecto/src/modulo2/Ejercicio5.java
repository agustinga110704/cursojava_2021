/* Cual de las siguientes l�neas dan errores de compilaci�n y para esos casos cubrirlos con el 
 * casteo correspondiente. */

package modulo2;

public class Ejercicio5 {

	public static void main(String[] args) {
		byte b=0;
		short s=1;
		int i=2;
		long l=3;
		
		i=s; //No hay errores.
		
		//b=s; Error de compilaci�n. Se debe corregir as�:
		b=(byte)s;
		
		l=i; //No hay errores.
		
		//b=i; Error de compilaci�n. Se debe corregir as�:
		b=(byte)i;
		
		//s=i; Error de compilaci�n. Se debe corregir as�:
		s=(short)i;
		
		System.out.println(b + "\n" + s + "\n" + i + "\n" + l);


	}

}
