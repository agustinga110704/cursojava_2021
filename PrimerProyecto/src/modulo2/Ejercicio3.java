/* Crear una clase identificada como Ejercicio3.java dentro del paquete modulo2, en dicha clase 
 * realizar un programa que permita alojar en  variables la siguiente información utilizando el 
 * tipo de dato que ocupe la menor cantidad de memoria posible. */

package modulo2;

public class Ejercicio3 {

	public static void main(String[] args) {
		//Los siguientes son los tipos de datos que menos memoria usan para cada caso:
		char division = 'b';
		byte cantidad_de_goles = 3;
		int capacidad_cancha = 70000;
		float promedio_goles = 2.68F;
		
		System.out.println("División: " + division);
		System.out.println("Cantidad de goles por partido: " + cantidad_de_goles);
		System.out.println("Capacidad de la cancha: " + capacidad_cancha);
		System.out.println("Promedio de goles: " + promedio_goles);
	}

}
