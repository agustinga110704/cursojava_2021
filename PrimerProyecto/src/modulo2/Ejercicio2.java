/* Crear una clase identificada como Mod2_Ejercicio2.java dentro del paquete modulo2, recuerde
 * tener chequeado el chekbox que permite crear el m�todo main, luego mostrar en pantalla todos 
 * los tipos de datos enteros con sus correspondientes m�ximos y m�nimos,  para ello, una variable 
 * para cada uno de  los casos como se muestra a continuaci�n. */

package modulo2;

public class Ejercicio2 {

	public static void main(String[] args) {
		//Tipos de datos de n�meros enteros.
		byte bmin = -128;
		byte bmax = 127;
		short smin = -32768;
		short smax = 32767;
		int imin = -2147483648;
		int imax = 2147483647;
		long lmin = -9223372036854775808L;
		long lmax = 9223372036854775807L;
		
		System.out.println("Tipo\tM�nimo\t\t\tM�ximo");
		System.out.println(".....\t....................\t...................");
		System.out.println("\nbyte\t" + bmin + "\t\t\t" + bmax);
		System.out.println("short\t" + smin + "\t\t\t" + smax);
		System.out.println("int\t" + imin + "\t\t" + imax);
		System.out.println("long\t" + lmin + "\t" + lmax + "\n");
		
		System.out.println("La f�rmula general para calcular los valores m�ximos y m�nimos es:");
		System.out.println("Valor m�nimo: -2^(Tama�o en bits del tipo de dato - 1)");
		System.out.println("Valor m�ximo: 2^(Tama�o en bits del tipo de dato - 1) - 1");
		
		

	}

}
